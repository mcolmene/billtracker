import { fromJS } from 'immutable';

const defaultState = fromJS({
  hasErrored: false,
  isLoading: false,
});

export function addItem(state = defaultState, action) {
  switch (action.type) {
    case 'ADD_ITEM_FAILURE': {
      return state.set('hasErrored', action.hasErrored);
    }
    case 'ADD_ITEM_PENDING': {
      return state.set('isLoading', action.isLoading);
    }
    default:
      return state;
  }
}
