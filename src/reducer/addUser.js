import { fromJS } from 'immutable';

const defaultState = fromJS({
  hasErrored: false,
  isLoading: false,
});

export function addUser(state = defaultState, action) {
  switch (action.type) {
    case 'ADD_USER_FAILURE': {
      return state.set('hasErrored', action.hasErrored);
    }
    case 'ADD_USER_PENDING': {
      return state.set('isLoading', action.isLoading);
    }
    case 'ADD_USER_SUCCESS': {
      return state;
    }
    default:
      return state;
  }
}
