import { fromJS } from 'immutable';

const defaultState = fromJS({
  hasErrored: false,
  isLoading: false,
  users: []
});

export function users(state = defaultState, action) {
  switch (action.type) {
    case 'FETCH_USERS_DATA_ERROR': {
      return state.set('hasErrored', action.hasErrored);
    }
    case 'USERS_ARE_LOADING': {
      return state.set('isLoading', action.isLoading);
    }
    case 'FETCH_USERS_DATA_SUCCESS': {
      return state.set('users', action.users);
    }
    default:
      return state;
  }
}
