import { fromJS } from 'immutable';

const defaultState = fromJS({
  hasErrored: false,
  isLoading: false,
  loginAttempted: false,
  validLogin: false
});

export function login(state = defaultState, action) {
  switch (action.type) {
    case 'LOGIN_FAILURE': {
      return state.set('hasErrored', action.hasErrored);
    }
    case 'LOGIN_PENDING': {
      return state.set('isLoading', action.isLoading);
    }
    case 'LOGIN_ATTEMPTED' : {
      return state.set('loginAttempted', action.loginAttempted);
    }
    case 'LOGIN_SUCCESS': {
      console.log(state);
      return (action.valid)
        ? state.set('validLogin', action.valid)
        : state.set('validLogin', action.valid);
    }
    default:
      return state;
  }
}
