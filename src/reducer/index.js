import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { users } from './users';
import { addUser } from './addUser';
import { login } from './login';
import { bills } from './bills';
import { addItem } from './addItem';

export default combineReducers({
  users,
  addUser,
  login,
  bills,
  addItem,
  routing: routerReducer
});
