import { Map } from 'immutable';

const defaultState = Map({
  hasErrored: false,
  isLoading: false,
  bills: []
});

export function bills(state = defaultState, action) {
  switch (action.type) {
    case 'FETCH_BILLS_DATA_ERROR': {
      return state.set('hasErrored', action.hasErrored);
    }
    case 'BILLS_ARE_LOADING': {
      return state.set('isLoading', action.isLoading);
    }
    case 'FETCH_BILLS_DATA_SUCCESS': {
      return state.set('bills', action.bills);
    }
    case 'ADD_ITEM_SUCCESS': {
      let billList = state.get('bills');
      billList.push(action.item);
      return state.set('bills', [...billList]);
    }
    default:
      return state;
  }
}
