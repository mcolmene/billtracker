const ADD_USER_PENDING = 'ADD_USER_PENDING';
const ADD_USER_FAILURE = 'ADD_USER_FAILURE';
const ADD_USER_SUCCESS = 'ADD_USER_SUCCESS';

export function addUserFailure(bool) {
  return {
    type: ADD_USER_FAILURE,
    hasErrored: bool
  };
}
export function addUserPending(bool) {
  return {
    type: ADD_USER_PENDING,
    isLoading: bool
  };
}
export function addUserSuccess() {
  return {
    type: ADD_USER_SUCCESS,
  };
}

export function addUser(url, username, password) {
  const payload = {
    username,
    password
  };
  const myInit = {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
  };
  return (dispatch) => {
    dispatch(addUserPending(true));
    fetch(url, myInit)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(addUserPending(false));
        return response;
      })
      .then((response) => response.json())
      .then((users) => dispatch(addUserSuccess(users)))
      .catch(() => dispatch(addUserFailure(true)));
  };
}
