const ADD_ITEM_PENDING = 'ADD_ITEM_PENDING';
const ADD_ITEM_FAILURE = 'ADD_ITEM_FAILURE';
const ADD_ITEM_SUCCESS = 'ADD_ITEM_SUCCESS';

export function addItemFailure(bool) {
  return {
    type: ADD_ITEM_FAILURE,
    hasErrored: bool
  };
}
export function addItemPending(bool) {
  return {
    type: ADD_ITEM_PENDING,
    isLoading: bool
  };
}
export function addItemSuccess(item) {
  return {
    type: ADD_ITEM_SUCCESS,
    item
  };
}

export function addItem(url, itemObject) {
  const payload = itemObject;
  const myInit = {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
  };
  return (dispatch) => {
    dispatch(addItemPending(true));
    fetch(url, myInit)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(addItemPending(false));
        return response;
      })
      .then((response) => response.json())
      .then((items) => dispatch(addItemSuccess(itemObject)))
      .catch(() => dispatch(addItemFailure(true)));
  };
}
