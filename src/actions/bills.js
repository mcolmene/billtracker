const FETCH_BILLS_DATA_ERROR = 'FETCH_BILLS_DATA_ERROR';
const BILLS_ARE_LOADING = 'BILLS_ARE_LOADING';
const FETCH_BILLS_DATA_SUCCESS = 'FETCH_BILLS_DATA_SUCCESS';

export function fetchBillsDataFailure(bool) {
  return {
    type: FETCH_BILLS_DATA_ERROR,
    hasErrored: bool
  };
}
export function billsAreLoading(bool) {
  return {
    type: BILLS_ARE_LOADING,
    isLoading: bool
  };
}
export function fetchBillsDataSuccess(bills) {
  return {
    type: FETCH_BILLS_DATA_SUCCESS,
    bills
  };
}

export function getBills(url) {
  return (dispatch) => {
    dispatch(billsAreLoading(true));
    fetch(url)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(billsAreLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((bills) => dispatch(fetchBillsDataSuccess(bills)))
      .catch(() => dispatch(fetchBillsDataFailure(true)));
  };
}

