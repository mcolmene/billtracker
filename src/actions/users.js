const FETCH_USERS_DATA_ERROR = 'FETCH_USERS_DATA_ERROR';
const USERS_ARE_LOADING = 'USERS_ARE_LOADING';
const FETCH_USERS_DATA_SUCCESS = 'FETCH_USERS_DATA_SUCCESS';

export function fetchUsersDataFailure(bool) {
  return {
    type: FETCH_USERS_DATA_ERROR,
    hasErrored: bool
  };
}
export function usersAreLoading(bool) {
  return {
    type: USERS_ARE_LOADING,
    isLoading: bool
  };
}
export function fetchUsersDataSuccess(users) {
  return {
    type: FETCH_USERS_DATA_SUCCESS,
    users
  };
}

export function getUsers(url) {
  return (dispatch) => {
    dispatch(usersAreLoading(true));
    fetch(url)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(usersAreLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((users) => dispatch(fetchUsersDataSuccess(users)))
      .catch(() => dispatch(fetchUsersDataFailure(true)));
  };
}
