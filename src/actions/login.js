const LOGIN_PENDING = 'LOGIN_PENDING';
const LOGIN_FAILURE = 'LOGIN_FAILURE';
const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
const LOGIN_ATTEMPTED = 'LOGIN_ATTEMPTED';
export function loginFailure(bool) {
  return {
    type: LOGIN_FAILURE,
    hasErrored: bool
  };
}
export function loginPending(bool) {
  return {
    type: LOGIN_PENDING,
    isLoading: bool
  };
}
export function loginAttempted(bool) {
  return {
    type: LOGIN_ATTEMPTED,
    loginAttempted: bool
  };
}
export function loginSuccess(bool) {
  return {
    type: LOGIN_SUCCESS,
    valid: bool
  };
}

export function login(url, username, password) {
  const payload = {
    username,
    password
  };
  const myInit = {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
  };
  return (dispatch) => {
    dispatch(loginPending(true));
    fetch(url, myInit)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(loginPending(false));
        return response;
      })
      .then((response) => response.json())
      .then((valid) => {
        (valid.credentials === 'valid') // eslint-disable-line
          ? dispatch(loginSuccess(true))
          : dispatch(loginSuccess(false));
      })
      .then((valid) => {
      console.log(valid);
      console.log('about to dispatch loginattempted')
        dispatch(loginAttempted(true))
      })
      .catch(() => dispatch(loginFailure(true)));
  };
}
