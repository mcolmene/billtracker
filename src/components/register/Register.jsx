import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {
  Button,
  Col,
  Row,
  Grid
} from 'react-bootstrap/lib';
import { mapStateToProps, mapDispatchToProps } from '../../config/Register.props';

import styles from '../../static/CrimsonPrime.css'

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: ''
    };
    // Lets store in state, props is supposed to be const type
    this.submit = this.submit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  submit() {
    this.props.addUser('http://localhost:3000/adduser', this.state.username, this.state.password);
    this.setState({
      username: '',
      password: ''
    });
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  render() {
    return (
      <Grid className={`${styles.loginContainer}`}>
        <Row className="center">
          <h1>Register</h1>
        </Row>
        <input
          type="text"
          placeholder="User Name"
          name="username"
          value={this.state.username}
          onChange={this.handleChange}
        />
        <input
          type="text"
          placeholder="Password"
          name="password"
          value={this.state.password}
          onChange={this.handleChange}
        />
        <button type="submit" onClick={this.submit}>Submit</button>
      </Grid>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Register);

Register.defaultProps = {

};

Register.propTypes = {
  addUser: PropTypes.func
};
