import React, { Component } from 'react'
import { Row, Col, Button } from 'react-bootstrap/lib';
import AddModal from '../modals/AddModal';
import styles from '../../static/Input.css';

export default class BillTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
    };
    this.open = this.open.bind(this);
    this.close = this.close.bind(this);
// Lets store in state, props is supposed to be const type
    this.submit = this.submit.bind(this);
  }
  componentWillMount() {
    this.props.getBills('http://www.fast-thicket-44176.herokuapp.com/bills', {mode: 'no-cors'});
  }
  open() {
    this.setState({ showModal: true });
  }
  close() {
    this.setState({ showModal: false });
  }
  submit(itemObj) {
    this.props.addItem('http://localhost:3000/bills', itemObj);
    this.close();
  }
  render() {
    const { bills } = this.props;
    const billContent = (bills)
      ? (bills.map((bill, index) => (<tr key={`${index}`}>
          <td>{bill.type}</td>
          <td>{bill.item}</td>
          <td>{bill.amount}</td>
          <td>{bill.description}</td>
          <td>{bill.date}</td>
          <td>{bill.user}</td>
        </tr>)
      ))
      : (null);
    return (
      <Col>
        <table className="table table-hover">
          <thead>
          <tr>
            <th>Type</th>
            <th>Item</th>
            <th>Amount</th>
            <th>Description</th>
            <th>Date</th>
            <th>Submitted By</th>
          </tr>
          </thead>
          <tbody>
          {
            billContent
          }
          </tbody>
        </table>
        <AddModal
          open={this.open}
          close={this.close}
          showModal={this.state.showModal}
          submit={this.submit}
          {...this.props}/>
      </Col>
    )
  }
}
