import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import {
  Button,
  Col,
  Row,
  Grid
} from 'react-bootstrap/lib';
import LoadingSpinner from 'base-components/build/LoadingSpinner';

import { mapStateToProps, mapDispatchToProps } from '../../config/Login.props';

import styles from '../../static/CrimsonPrime.css'

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      usernameLabel: '',
      password: '',
      passwordLabel: '',
    };
    // Lets store in state, props is supposed to be const type
    this.submit = this.submit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.handleBlur = this.handleBlur.bind(this);

  }

  getValidationState(type) {
    const userLength = this.state.username.length;
    const passLength = this.state.password.length;
    switch (type) {
      case 'USERNAME' : {
        if (userLength > 10) return 'success';
        else if (userLength > 5) return 'warning';
        else if (userLength > 0) return 'error';
        break;
      }
      case 'PASSWORD' : {
        if (passLength > 10) return 'success';
        else if (passLength > 5) return 'warning';
        else if (passLength > 0) return 'error';
        break;
      }
      default : {
        return false;
      }
    }
    return false;
  }
  submit() {
    this.props.login('http://localhost:3000/login', this.state.username, this.state.password);
    this.setState({
      username: '',
      usernameLabel: '',
      password: '',
      passwordLabel: ''
    });
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }
  handleFocus(event) {
    const label = event.target.name + 'Label';
    this.setState({ [label]: styles.transform });
  }
  handleBlur(event) {
    if(this.state[event.target.name].length === 0) {
      const label = event.target.name + 'Label';
      this.setState({[label]: ''});
    }
  }
  render() {
    const { isLoading, hasErrored, validLogin, loginAttempted } = this.props;
    let WarningMessage = () => (null);
    const submitText = () => {
    if (hasErrored) {
      return <span>Error</span>;
    } else if (isLoading) {
      return (<div>
          <span>Submit</span>
          <LoadingSpinner />
        </div>
      );
    }
      return <span>Submit</span>;
    };
    if(!isLoading && loginAttempted && !validLogin) {
      WarningMessage = () => (<p className={`${styles.warning}`}>The username and password does not match our records</p>);
    }
    return (
      <Grid className={`${styles.loginContainer}`}>
        <Row className="center">
          <h1>Login</h1>
        </Row>
        <Col sm={6} md={6} className={`${styles.loginForm} ${styles['border-r']}`}>
          <WarningMessage />
          <Row>
          <Col sm={11} md={11} className="pad-lr-4">
            <label className={this.state.usernameLabel}>User Name</label>
              <input
                type="text"
                name="username"
                className=""
                value={this.state.username}
                onChange={this.handleChange}
                onFocus={this.handleFocus}
                onBlur={this.handleBlur}
              />
          </Col>
          </Row>
          <Row>
          <Col sm={11} md={11} className="pad-lr-4">
            <label className={this.state.passwordLabel}>Password</label>
            <input
              type="password"
              name="password"
              className=""
              value={this.state.password}
              onChange={this.handleChange}
              onFocus={this.handleFocus}
              onBlur={this.handleBlur}
            />
          </Col>
          </Row>
          <Row>
            <Col sm={6} md={6}>
            </Col>
            <Col sm={6} md={6} className="center pad-3">
              <span>Forgot Password?</span>
            </Col>
          </Row>
          <Row className="center">
            <button className={`${styles.submitButton} margin-t-2`} type="submit" onClick={this.submit}>
              {
                submitText()
              }
            </button>
            <Link to="/register"><p className="pad-2">Create a new account</p></Link>
          </Row>
        </Col>
        <Col sm={6} md={6} className="center">
          <p>Log in with one of the following social media services</p>
          <div className={`${styles.socialMediaContainer}`}>
            <button className={`${styles.facebookBox}`}>Facebook</button>
            <button className={`${styles.gmailBox}`}>Gmail</button>
          </div>
        </Col>
      </Grid>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);

Login.defaultProps = {
  hasErrored: false,
  isLoading: false,
};

Login.propTypes = {
  hasErrored: PropTypes.bool,
  isLoading: PropTypes.bool,
  login: PropTypes.func
};
