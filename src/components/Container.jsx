import React, { Component, PropTypes } from 'react';
import { Row, Col } from 'react-bootstrap/lib';
import { Link } from 'react-router';

import styles from '../static/CrimsonPrime.css';

export default class Container extends Component {
  render() {
    return (
      <div>
        <Row className={`${styles.menuBar}`}>
          <Col sm={10} md={10} className="center float-l">
            <Col sm={3} md={2}>
              <Link to="/" className="">Bill Tracker</Link>
            </Col>
          </Col>
          <Col sm={2} md={2} className="right float-r">
            <Link to="/login" className="">Login</Link>
          </Col>
        </Row>
        { React.cloneElement(this.props.children) }
      </div>
    );
  }
}
Container.defaultProps = {

};

Container.propTypes = {
  children: PropTypes.element
};

