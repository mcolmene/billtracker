import React, { Component } from 'react';
import { Button, Modal, Col, Row} from 'react-bootstrap/lib';
import styles from '../../static/Input.css';

function formatDate(date) {
  const dateArray = date.split(' ');
  const fmtDate = dateArray[0] + ' ' + dateArray[1] + ' ' + dateArray[2] + ' ' + dateArray[3];
  return fmtDate;
}

export default class AddModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: {
        value: '',
        inputType: 'select',
        options: [
          'Bills',
          'Other'
        ]
      },
      item: {
        value:'',
        inputType: 'input'
      },
      itemLabel: '',
      amount: {
        value:'',
        inputType: 'input'
      },
      amountLabel: '',
      description: {
        value:'',
        inputType: 'input'
      },
      descriptionLabel: '',
      user: {
        value:'',
        inputType: 'select',
        options: [
          'Martin',
          'Vanessa'
        ],
      },
      userLabel: ''
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.submitClick = this.submitClick.bind(this);
  }
  handleChange(event) {
    console.log(event.target.value)
    const newObj = {
      value : event.target.value
    };
    console.log(newObj);

    const mergedObj = Object.assign({}, this.state[event.target.name], newObj);
    console.log(mergedObj);
    this.setState({ [event.target.name]: mergedObj });
  }
  handleFocus(event) {
    const label = event.target.name + 'Label';
    this.setState({ [label]: styles.transform });
  }
  handleBlur(event) {
    if(this.state[event.target.name].length === 0) {
      const label = event.target.name + 'Label';
      this.setState({[label]: ''});
    }
  }
  createFormInput(stateObject, key, index) {
    let content;
    switch(stateObject.inputType) {
      case 'select': {
        content = (
          <Col key={`${key}Div`} sm={11} md={12} className="pad-lr-4">
            <select
              className="form-control"
              onChange={this.handleChange}
              value={'Martin'}
            >
              {
                stateObject.options.map((option) => (
                  <option value={option}>{option}</option>
                ))
              }
            </select>
          </Col>
        );
        break;
      }
      case 'input': {
        content = (
          <Col key={`${key}Div`} sm={11} md={12} className="pad-lr-4">
            <label className={this.state[`${key}Label`]}>{key}</label>
            <input
              key={`input${index}`}
              type="text"
              name={key}
              className=""
              value={this.state.key}
              onChange={this.handleChange}
              onFocus={this.handleFocus}
              onBlur={this.handleBlur}
            />
          </Col>
        );
        break;
      }
      default: {
        content = <div></div>;
        break;
      }
    }
    return content;
  }
  submitClick() {
    const { type, item, amount, description, user } = this.state;
    console.log(type);
    console.log(type.value);
    const itemObj = {
      type: type.value,
      item: item.value,
      amount: amount.value,
      description: description.value,
      date: formatDate(new Date().toString()),
      user: user.value
    };
    this.props.submit(itemObj);
    this.setState({
      type: {
        value: '',
        inputType: 'select',
        options: [
          'Bills',
          'Other'
        ]
      },
      item: {
        value: '',
        inputType: 'input'
      },
      itemLabel: '',
      amount: {
        value: '',
        inputType: 'input'
      },
      amountLabel: '',
      description: {
        value: '',
        inputType: 'input'
      },
      descriptionLabel: '',
      user: {
        value: '',
        inputType: 'select',
        options: [
          'Martin',
          'Vanessa'
        ],
      },
      userLabel: ''
    });
  };
  render() {
    const {
      close,
      showModal
    } = this.props;
    console.log(this.state);
    const inputs = (
      Object.keys(this.state).map((key, index) => {
        const content = (key.includes('Label'))
          ? (null)
          : (
            this.createFormInput(this.state[key], key, index)
          );
        return content;
      })
    );
    return(
      <div>
        <Button
          bsStyle="primary"
          bsSize="large"
          onClick={this.props.open}
        >
          Add
        </Button>

        <Modal show={showModal} onHide={close}>
          <Modal.Header closeButton>
            <Modal.Title>Add an Item</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row className={`${styles.inputContainer} ${styles['border-r']}`}>
            {
              inputs
            }
            </Row>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.submitClick}>Submit</Button>
            <Button onClick={close}>Close</Button>
          </Modal.Footer>
        </Modal>
      </div>
    )
  }
}
