import React, { Component } from 'react';
import { connect } from 'react-redux';
// import { Link } from 'react-router';

// components from node_modules
import { Row, Col } from 'react-bootstrap/lib';
// import  LoadingSpinner  from 'base-components/build/LoadingSpinner';
import DetailsBlock from 'base-components/build/DetailsBlock';
import ImageSection from 'base-components/build/ImageSection';

// custom components
// import Register from './Register';
// import Login from './Login';
import BillTable from './table/BillTable';
import { Grid } from 'react-bootstrap/lib';

import { mapStateToProps, mapDispatchToProps } from '../config/BillTracker.props';

import styles from '../static/CrimsonPrime.css';

class BillTracker extends Component {

  render() {
    return (
      <Grid className="margin-t-2">
        <BillTable {...this.props}/>
      </Grid>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BillTracker);
