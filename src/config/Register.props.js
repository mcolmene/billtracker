import { addUser } from '../actions/addUser';

export const mapStateToProps = (state) => {
  const { hasErrored, isLoading } = state.addUser.toJS();
  return {
    hasErrored,
    isLoading
  };
};

export const mapDispatchToProps = (dispatch) => ({
  addUser: (url, username, password) => dispatch(addUser(url, username, password))
});

