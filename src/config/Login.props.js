import { login } from '../actions/login';
import { fromJS } from 'immutable';
export const mapStateToProps = (state) => {
  const { hasErrored, isLoading, validLogin, loginAttempted } = state.login.toJS();
  return {
    hasErrored,
    isLoading,
    loginAttempted,
    validLogin
  };
};

export const mapDispatchToProps = (dispatch) => ({
  login: (url, username, password) => dispatch(login(url, username, password))
});

