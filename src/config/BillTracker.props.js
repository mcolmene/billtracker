import { getBills } from '../actions/bills';
import { addItem } from '../actions/addItem';

export const mapStateToProps = (state) => {
  const { bills, hasErrored, isLoading } = state.bills.toJS();
  return {
    bills,
    hasErrored,
    isLoading
  };
};

export const mapDispatchToProps = (dispatch) => ({
  getBills: (url) => dispatch(getBills(url)),
  addItem: (url, itemObject) => dispatch(addItem(url, itemObject))
});

