'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.fetchUsersDataFailure = fetchUsersDataFailure;
exports.usersAreLoading = usersAreLoading;
exports.fetchUsersDataSuccess = fetchUsersDataSuccess;
exports.getUsers = getUsers;
var FETCH_USERS_DATA_ERROR = 'FETCH_USERS_DATA_ERROR';
var USERS_ARE_LOADING = 'USERS_ARE_LOADING';
var FETCH_USERS_DATA_SUCCESS = 'FETCH_USERS_DATA_SUCCESS';

function fetchUsersDataFailure(bool) {
  return {
    type: FETCH_USERS_DATA_ERROR,
    hasErrored: bool
  };
}
function usersAreLoading(bool) {
  return {
    type: USERS_ARE_LOADING,
    isLoading: bool
  };
}
function fetchUsersDataSuccess(users) {
  return {
    type: FETCH_USERS_DATA_SUCCESS,
    users: users
  };
}

function getUsers(url) {
  return function (dispatch) {
    dispatch(usersAreLoading(true));
    fetch(url).then(function (response) {
      if (!response.ok) {
        throw Error(response.statusText);
      }
      dispatch(usersAreLoading(false));
      return response;
    }).then(function (response) {
      return response.json();
    }).then(function (users) {
      return dispatch(fetchUsersDataSuccess(users));
    }).catch(function () {
      return dispatch(fetchUsersDataFailure(true));
    });
  };
}