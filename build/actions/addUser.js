'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.addUserFailure = addUserFailure;
exports.addUserPending = addUserPending;
exports.addUserSuccess = addUserSuccess;
exports.addUser = addUser;
var ADD_USER_PENDING = 'ADD_USER_PENDING';
var ADD_USER_FAILURE = 'ADD_USER_FAILURE';
var ADD_USER_SUCCESS = 'ADD_USER_SUCCESS';

function addUserFailure(bool) {
  return {
    type: ADD_USER_FAILURE,
    hasErrored: bool
  };
}
function addUserPending(bool) {
  return {
    type: ADD_USER_PENDING,
    isLoading: bool
  };
}
function addUserSuccess() {
  return {
    type: ADD_USER_SUCCESS
  };
}

function addUser(url, username, password) {
  var payload = {
    username: username,
    password: password
  };
  var myInit = {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
  };
  return function (dispatch) {
    dispatch(addUserPending(true));
    fetch(url, myInit).then(function (response) {
      if (!response.ok) {
        throw Error(response.statusText);
      }
      dispatch(addUserPending(false));
      return response;
    }).then(function (response) {
      return response.json();
    }).then(function (users) {
      return dispatch(addUserSuccess(users));
    }).catch(function () {
      return dispatch(addUserFailure(true));
    });
  };
}