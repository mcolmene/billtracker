'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.loginFailure = loginFailure;
exports.loginPending = loginPending;
exports.loginAttempted = loginAttempted;
exports.loginSuccess = loginSuccess;
exports.login = login;
var LOGIN_PENDING = 'LOGIN_PENDING';
var LOGIN_FAILURE = 'LOGIN_FAILURE';
var LOGIN_SUCCESS = 'LOGIN_SUCCESS';
var LOGIN_ATTEMPTED = 'LOGIN_ATTEMPTED';
function loginFailure(bool) {
  return {
    type: LOGIN_FAILURE,
    hasErrored: bool
  };
}
function loginPending(bool) {
  return {
    type: LOGIN_PENDING,
    isLoading: bool
  };
}
function loginAttempted(bool) {
  return {
    type: LOGIN_ATTEMPTED,
    loginAttempted: bool
  };
}
function loginSuccess(bool) {
  return {
    type: LOGIN_SUCCESS,
    valid: bool
  };
}

function login(url, username, password) {
  var payload = {
    username: username,
    password: password
  };
  var myInit = {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
  };
  return function (dispatch) {
    dispatch(loginPending(true));
    fetch(url, myInit).then(function (response) {
      if (!response.ok) {
        throw Error(response.statusText);
      }
      dispatch(loginPending(false));
      return response;
    }).then(function (response) {
      return response.json();
    }).then(function (valid) {
      valid.credentials === 'valid' ? // eslint-disable-line
      dispatch(loginSuccess(true)) : dispatch(loginSuccess(false));
    }).then(function (valid) {
      console.log(valid);
      console.log('about to dispatch loginattempted');
      dispatch(loginAttempted(true));
    }).catch(function () {
      return dispatch(loginFailure(true));
    });
  };
}