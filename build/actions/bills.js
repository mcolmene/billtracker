'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.fetchBillsDataFailure = fetchBillsDataFailure;
exports.billsAreLoading = billsAreLoading;
exports.fetchBillsDataSuccess = fetchBillsDataSuccess;
exports.getBills = getBills;
var FETCH_BILLS_DATA_ERROR = 'FETCH_BILLS_DATA_ERROR';
var BILLS_ARE_LOADING = 'BILLS_ARE_LOADING';
var FETCH_BILLS_DATA_SUCCESS = 'FETCH_BILLS_DATA_SUCCESS';

function fetchBillsDataFailure(bool) {
  return {
    type: FETCH_BILLS_DATA_ERROR,
    hasErrored: bool
  };
}
function billsAreLoading(bool) {
  return {
    type: BILLS_ARE_LOADING,
    isLoading: bool
  };
}
function fetchBillsDataSuccess(bills) {
  return {
    type: FETCH_BILLS_DATA_SUCCESS,
    bills: bills
  };
}

function getBills(url) {
  return function (dispatch) {
    dispatch(billsAreLoading(true));
    fetch(url).then(function (response) {
      if (!response.ok) {
        throw Error(response.statusText);
      }
      dispatch(billsAreLoading(false));
      return response;
    }).then(function (response) {
      return response.json();
    }).then(function (bills) {
      return dispatch(fetchBillsDataSuccess(bills));
    }).catch(function () {
      return dispatch(fetchBillsDataFailure(true));
    });
  };
}