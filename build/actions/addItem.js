'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.addItemFailure = addItemFailure;
exports.addItemPending = addItemPending;
exports.addItemSuccess = addItemSuccess;
exports.addItem = addItem;
var ADD_ITEM_PENDING = 'ADD_ITEM_PENDING';
var ADD_ITEM_FAILURE = 'ADD_ITEM_FAILURE';
var ADD_ITEM_SUCCESS = 'ADD_ITEM_SUCCESS';

function addItemFailure(bool) {
  return {
    type: ADD_ITEM_FAILURE,
    hasErrored: bool
  };
}
function addItemPending(bool) {
  return {
    type: ADD_ITEM_PENDING,
    isLoading: bool
  };
}
function addItemSuccess(item) {
  return {
    type: ADD_ITEM_SUCCESS,
    item: item
  };
}

function addItem(url, itemObject) {
  var payload = itemObject;
  var myInit = {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
  };
  return function (dispatch) {
    dispatch(addItemPending(true));
    fetch(url, myInit).then(function (response) {
      if (!response.ok) {
        throw Error(response.statusText);
      }
      dispatch(addItemPending(false));
      return response;
    }).then(function (response) {
      return response.json();
    }).then(function (items) {
      return dispatch(addItemSuccess(itemObject));
    }).catch(function () {
      return dispatch(addItemFailure(true));
    });
  };
}