'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _reactRouter = require('react-router');

var _lib = require('react-bootstrap/lib');

var _LoadingSpinner = require('base-components/build/LoadingSpinner');

var _LoadingSpinner2 = _interopRequireDefault(_LoadingSpinner);

var _Login = require('../../config/Login.props');

var _CrimsonPrime = require('../../static/CrimsonPrime.css');

var _CrimsonPrime2 = _interopRequireDefault(_CrimsonPrime);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Login = function (_Component) {
  _inherits(Login, _Component);

  function Login(props) {
    _classCallCheck(this, Login);

    var _this = _possibleConstructorReturn(this, (Login.__proto__ || Object.getPrototypeOf(Login)).call(this, props));

    _this.state = {
      username: '',
      usernameLabel: '',
      password: '',
      passwordLabel: ''
    };
    // Lets store in state, props is supposed to be const type
    _this.submit = _this.submit.bind(_this);
    _this.handleChange = _this.handleChange.bind(_this);
    _this.handleFocus = _this.handleFocus.bind(_this);
    _this.handleBlur = _this.handleBlur.bind(_this);

    return _this;
  }

  _createClass(Login, [{
    key: 'getValidationState',
    value: function getValidationState(type) {
      var userLength = this.state.username.length;
      var passLength = this.state.password.length;
      switch (type) {
        case 'USERNAME':
          {
            if (userLength > 10) return 'success';else if (userLength > 5) return 'warning';else if (userLength > 0) return 'error';
            break;
          }
        case 'PASSWORD':
          {
            if (passLength > 10) return 'success';else if (passLength > 5) return 'warning';else if (passLength > 0) return 'error';
            break;
          }
        default:
          {
            return false;
          }
      }
      return false;
    }
  }, {
    key: 'submit',
    value: function submit() {
      this.props.login('http://localhost:3000/login', this.state.username, this.state.password);
      this.setState({
        username: '',
        usernameLabel: '',
        password: '',
        passwordLabel: ''
      });
    }
  }, {
    key: 'handleChange',
    value: function handleChange(event) {
      this.setState(_defineProperty({}, event.target.name, event.target.value));
    }
  }, {
    key: 'handleFocus',
    value: function handleFocus(event) {
      var label = event.target.name + 'Label';
      this.setState(_defineProperty({}, label, _CrimsonPrime2.default.transform));
    }
  }, {
    key: 'handleBlur',
    value: function handleBlur(event) {
      if (this.state[event.target.name].length === 0) {
        var label = event.target.name + 'Label';
        this.setState(_defineProperty({}, label, ''));
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          isLoading = _props.isLoading,
          hasErrored = _props.hasErrored,
          validLogin = _props.validLogin,
          loginAttempted = _props.loginAttempted;

      var WarningMessage = function WarningMessage() {
        return null;
      };
      var submitText = function submitText() {
        if (hasErrored) {
          return _react2.default.createElement(
            'span',
            null,
            'Error'
          );
        } else if (isLoading) {
          return _react2.default.createElement(
            'div',
            null,
            _react2.default.createElement(
              'span',
              null,
              'Submit'
            ),
            _react2.default.createElement(_LoadingSpinner2.default, null)
          );
        }
        return _react2.default.createElement(
          'span',
          null,
          'Submit'
        );
      };
      if (!isLoading && loginAttempted && !validLogin) {
        WarningMessage = function WarningMessage() {
          return _react2.default.createElement(
            'p',
            { className: '' + _CrimsonPrime2.default.warning },
            'The username and password does not match our records'
          );
        };
      }
      return _react2.default.createElement(
        _lib.Grid,
        { className: '' + _CrimsonPrime2.default.loginContainer },
        _react2.default.createElement(
          _lib.Row,
          { className: 'center' },
          _react2.default.createElement(
            'h1',
            null,
            'Login'
          )
        ),
        _react2.default.createElement(
          _lib.Col,
          { sm: 6, md: 6, className: _CrimsonPrime2.default.loginForm + ' ' + _CrimsonPrime2.default['border-r'] },
          _react2.default.createElement(WarningMessage, null),
          _react2.default.createElement(
            _lib.Row,
            null,
            _react2.default.createElement(
              _lib.Col,
              { sm: 11, md: 11, className: 'pad-lr-4' },
              _react2.default.createElement(
                'label',
                { className: this.state.usernameLabel },
                'User Name'
              ),
              _react2.default.createElement('input', {
                type: 'text',
                name: 'username',
                className: '',
                value: this.state.username,
                onChange: this.handleChange,
                onFocus: this.handleFocus,
                onBlur: this.handleBlur
              })
            )
          ),
          _react2.default.createElement(
            _lib.Row,
            null,
            _react2.default.createElement(
              _lib.Col,
              { sm: 11, md: 11, className: 'pad-lr-4' },
              _react2.default.createElement(
                'label',
                { className: this.state.passwordLabel },
                'Password'
              ),
              _react2.default.createElement('input', {
                type: 'password',
                name: 'password',
                className: '',
                value: this.state.password,
                onChange: this.handleChange,
                onFocus: this.handleFocus,
                onBlur: this.handleBlur
              })
            )
          ),
          _react2.default.createElement(
            _lib.Row,
            null,
            _react2.default.createElement(_lib.Col, { sm: 6, md: 6 }),
            _react2.default.createElement(
              _lib.Col,
              { sm: 6, md: 6, className: 'center pad-3' },
              _react2.default.createElement(
                'span',
                null,
                'Forgot Password?'
              )
            )
          ),
          _react2.default.createElement(
            _lib.Row,
            { className: 'center' },
            _react2.default.createElement(
              'button',
              { className: _CrimsonPrime2.default.submitButton + ' margin-t-2', type: 'submit', onClick: this.submit },
              submitText()
            ),
            _react2.default.createElement(
              _reactRouter.Link,
              { to: '/register' },
              _react2.default.createElement(
                'p',
                { className: 'pad-2' },
                'Create a new account'
              )
            )
          )
        ),
        _react2.default.createElement(
          _lib.Col,
          { sm: 6, md: 6, className: 'center' },
          _react2.default.createElement(
            'p',
            null,
            'Log in with one of the following social media services'
          ),
          _react2.default.createElement(
            'div',
            { className: '' + _CrimsonPrime2.default.socialMediaContainer },
            _react2.default.createElement(
              'button',
              { className: '' + _CrimsonPrime2.default.facebookBox },
              'Facebook'
            ),
            _react2.default.createElement(
              'button',
              { className: '' + _CrimsonPrime2.default.gmailBox },
              'Gmail'
            )
          )
        )
      );
    }
  }]);

  return Login;
}(_react.Component);

exports.default = (0, _reactRedux.connect)(_Login.mapStateToProps, _Login.mapDispatchToProps)(Login);


Login.defaultProps = {
  hasErrored: false,
  isLoading: false
};

Login.propTypes = {
  hasErrored: _react.PropTypes.bool,
  isLoading: _react.PropTypes.bool,
  login: _react.PropTypes.func
};