'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lib = require('react-bootstrap/lib');

var _Input = require('../../static/Input.css');

var _Input2 = _interopRequireDefault(_Input);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function formatDate(date) {
  var dateArray = date.split(' ');
  var fmtDate = dateArray[0] + ' ' + dateArray[1] + ' ' + dateArray[2] + ' ' + dateArray[3];
  return fmtDate;
}

var AddModal = function (_Component) {
  _inherits(AddModal, _Component);

  function AddModal(props) {
    _classCallCheck(this, AddModal);

    var _this = _possibleConstructorReturn(this, (AddModal.__proto__ || Object.getPrototypeOf(AddModal)).call(this, props));

    _this.state = {
      type: {
        value: '',
        inputType: 'select',
        options: ['Bills', 'Other']
      },
      item: {
        value: '',
        inputType: 'input'
      },
      itemLabel: '',
      amount: {
        value: '',
        inputType: 'input'
      },
      amountLabel: '',
      description: {
        value: '',
        inputType: 'input'
      },
      descriptionLabel: '',
      user: {
        value: '',
        inputType: 'select',
        options: ['Martin', 'Vanessa']
      },
      userLabel: ''
    };
    _this.handleChange = _this.handleChange.bind(_this);
    _this.handleFocus = _this.handleFocus.bind(_this);
    _this.handleBlur = _this.handleBlur.bind(_this);
    _this.submitClick = _this.submitClick.bind(_this);
    return _this;
  }

  _createClass(AddModal, [{
    key: 'handleChange',
    value: function handleChange(event) {
      console.log(event.target.value);
      var newObj = {
        value: event.target.value
      };
      console.log(newObj);

      var mergedObj = Object.assign({}, this.state[event.target.name], newObj);
      console.log(mergedObj);
      this.setState(_defineProperty({}, event.target.name, mergedObj));
    }
  }, {
    key: 'handleFocus',
    value: function handleFocus(event) {
      var label = event.target.name + 'Label';
      this.setState(_defineProperty({}, label, _Input2.default.transform));
    }
  }, {
    key: 'handleBlur',
    value: function handleBlur(event) {
      if (this.state[event.target.name].length === 0) {
        var label = event.target.name + 'Label';
        this.setState(_defineProperty({}, label, ''));
      }
    }
  }, {
    key: 'createFormInput',
    value: function createFormInput(stateObject, key, index) {
      var content = void 0;
      switch (stateObject.inputType) {
        case 'select':
          {
            content = _react2.default.createElement(
              _lib.Col,
              { key: key + 'Div', sm: 11, md: 12, className: 'pad-lr-4' },
              _react2.default.createElement(
                'select',
                {
                  className: 'form-control',
                  onChange: this.handleChange,
                  value: 'Martin'
                },
                stateObject.options.map(function (option) {
                  return _react2.default.createElement(
                    'option',
                    { value: option },
                    option
                  );
                })
              )
            );
            break;
          }
        case 'input':
          {
            content = _react2.default.createElement(
              _lib.Col,
              { key: key + 'Div', sm: 11, md: 12, className: 'pad-lr-4' },
              _react2.default.createElement(
                'label',
                { className: this.state[key + 'Label'] },
                key
              ),
              _react2.default.createElement('input', {
                key: 'input' + index,
                type: 'text',
                name: key,
                className: '',
                value: this.state.key,
                onChange: this.handleChange,
                onFocus: this.handleFocus,
                onBlur: this.handleBlur
              })
            );
            break;
          }
        default:
          {
            content = _react2.default.createElement('div', null);
            break;
          }
      }
      return content;
    }
  }, {
    key: 'submitClick',
    value: function submitClick() {
      var _state = this.state,
          type = _state.type,
          item = _state.item,
          amount = _state.amount,
          description = _state.description,
          user = _state.user;

      console.log(type);
      console.log(type.value);
      var itemObj = {
        type: type.value,
        item: item.value,
        amount: amount.value,
        description: description.value,
        date: formatDate(new Date().toString()),
        user: user.value
      };
      this.props.submit(itemObj);
      this.setState({
        type: {
          value: '',
          inputType: 'select',
          options: ['Bills', 'Other']
        },
        item: {
          value: '',
          inputType: 'input'
        },
        itemLabel: '',
        amount: {
          value: '',
          inputType: 'input'
        },
        amountLabel: '',
        description: {
          value: '',
          inputType: 'input'
        },
        descriptionLabel: '',
        user: {
          value: '',
          inputType: 'select',
          options: ['Martin', 'Vanessa']
        },
        userLabel: ''
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          close = _props.close,
          showModal = _props.showModal;

      console.log(this.state);
      var inputs = Object.keys(this.state).map(function (key, index) {
        var content = key.includes('Label') ? null : _this2.createFormInput(_this2.state[key], key, index);
        return content;
      });
      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          _lib.Button,
          {
            bsStyle: 'primary',
            bsSize: 'large',
            onClick: this.props.open
          },
          'Add'
        ),
        _react2.default.createElement(
          _lib.Modal,
          { show: showModal, onHide: close },
          _react2.default.createElement(
            _lib.Modal.Header,
            { closeButton: true },
            _react2.default.createElement(
              _lib.Modal.Title,
              null,
              'Add an Item'
            )
          ),
          _react2.default.createElement(
            _lib.Modal.Body,
            null,
            _react2.default.createElement(
              _lib.Row,
              { className: _Input2.default.inputContainer + ' ' + _Input2.default['border-r'] },
              inputs
            )
          ),
          _react2.default.createElement(
            _lib.Modal.Footer,
            null,
            _react2.default.createElement(
              _lib.Button,
              { onClick: this.submitClick },
              'Submit'
            ),
            _react2.default.createElement(
              _lib.Button,
              { onClick: close },
              'Close'
            )
          )
        )
      );
    }
  }]);

  return AddModal;
}(_react.Component);

exports.default = AddModal;