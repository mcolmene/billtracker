'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lib = require('react-bootstrap/lib');

var _AddModal = require('../modals/AddModal');

var _AddModal2 = _interopRequireDefault(_AddModal);

var _Input = require('../../static/Input.css');

var _Input2 = _interopRequireDefault(_Input);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var BillTable = function (_Component) {
  _inherits(BillTable, _Component);

  function BillTable(props) {
    _classCallCheck(this, BillTable);

    var _this = _possibleConstructorReturn(this, (BillTable.__proto__ || Object.getPrototypeOf(BillTable)).call(this, props));

    _this.state = {
      showModal: false
    };
    _this.open = _this.open.bind(_this);
    _this.close = _this.close.bind(_this);
    // Lets store in state, props is supposed to be const type
    _this.submit = _this.submit.bind(_this);
    return _this;
  }

  _createClass(BillTable, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      this.props.getBills('http://www.fast-thicket-44176.herokuapp.com/bills', { mode: 'no-cors' });
    }
  }, {
    key: 'open',
    value: function open() {
      this.setState({ showModal: true });
    }
  }, {
    key: 'close',
    value: function close() {
      this.setState({ showModal: false });
    }
  }, {
    key: 'submit',
    value: function submit(itemObj) {
      this.props.addItem('http://localhost:3000/bills', itemObj);
      this.close();
    }
  }, {
    key: 'render',
    value: function render() {
      var bills = this.props.bills;

      var billContent = bills ? bills.map(function (bill, index) {
        return _react2.default.createElement(
          'tr',
          { key: '' + index },
          _react2.default.createElement(
            'td',
            null,
            bill.type
          ),
          _react2.default.createElement(
            'td',
            null,
            bill.item
          ),
          _react2.default.createElement(
            'td',
            null,
            bill.amount
          ),
          _react2.default.createElement(
            'td',
            null,
            bill.description
          ),
          _react2.default.createElement(
            'td',
            null,
            bill.date
          ),
          _react2.default.createElement(
            'td',
            null,
            bill.user
          )
        );
      }) : null;
      return _react2.default.createElement(
        _lib.Col,
        null,
        _react2.default.createElement(
          'table',
          { className: 'table table-hover' },
          _react2.default.createElement(
            'thead',
            null,
            _react2.default.createElement(
              'tr',
              null,
              _react2.default.createElement(
                'th',
                null,
                'Type'
              ),
              _react2.default.createElement(
                'th',
                null,
                'Item'
              ),
              _react2.default.createElement(
                'th',
                null,
                'Amount'
              ),
              _react2.default.createElement(
                'th',
                null,
                'Description'
              ),
              _react2.default.createElement(
                'th',
                null,
                'Date'
              ),
              _react2.default.createElement(
                'th',
                null,
                'Submitted By'
              )
            )
          ),
          _react2.default.createElement(
            'tbody',
            null,
            billContent
          )
        ),
        _react2.default.createElement(_AddModal2.default, _extends({
          open: this.open,
          close: this.close,
          showModal: this.state.showModal,
          submit: this.submit
        }, this.props))
      );
    }
  }]);

  return BillTable;
}(_react.Component);

exports.default = BillTable;