'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _lib = require('react-bootstrap/lib');

var _DetailsBlock = require('base-components/build/DetailsBlock');

var _DetailsBlock2 = _interopRequireDefault(_DetailsBlock);

var _ImageSection = require('base-components/build/ImageSection');

var _ImageSection2 = _interopRequireDefault(_ImageSection);

var _BillTable = require('./table/BillTable');

var _BillTable2 = _interopRequireDefault(_BillTable);

var _BillTracker = require('../config/BillTracker.props');

var _CrimsonPrime = require('../static/CrimsonPrime.css');

var _CrimsonPrime2 = _interopRequireDefault(_CrimsonPrime);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
// import { Link } from 'react-router';

// components from node_modules

// import  LoadingSpinner  from 'base-components/build/LoadingSpinner';


// custom components
// import Register from './Register';
// import Login from './Login';


var BillTracker = function (_Component) {
  _inherits(BillTracker, _Component);

  function BillTracker() {
    _classCallCheck(this, BillTracker);

    return _possibleConstructorReturn(this, (BillTracker.__proto__ || Object.getPrototypeOf(BillTracker)).apply(this, arguments));
  }

  _createClass(BillTracker, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        _lib.Grid,
        { className: 'margin-t-2' },
        _react2.default.createElement(_BillTable2.default, this.props)
      );
    }
  }]);

  return BillTracker;
}(_react.Component);

exports.default = (0, _reactRedux.connect)(_BillTracker.mapStateToProps, _BillTracker.mapDispatchToProps)(BillTracker);