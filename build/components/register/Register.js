'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _lib = require('react-bootstrap/lib');

var _Register = require('../../config/Register.props');

var _CrimsonPrime = require('../../static/CrimsonPrime.css');

var _CrimsonPrime2 = _interopRequireDefault(_CrimsonPrime);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Register = function (_Component) {
  _inherits(Register, _Component);

  function Register(props) {
    _classCallCheck(this, Register);

    var _this = _possibleConstructorReturn(this, (Register.__proto__ || Object.getPrototypeOf(Register)).call(this, props));

    _this.state = {
      username: '',
      password: ''
    };
    // Lets store in state, props is supposed to be const type
    _this.submit = _this.submit.bind(_this);
    _this.handleChange = _this.handleChange.bind(_this);
    return _this;
  }

  _createClass(Register, [{
    key: 'submit',
    value: function submit() {
      this.props.addUser('http://localhost:3000/adduser', this.state.username, this.state.password);
      this.setState({
        username: '',
        password: ''
      });
    }
  }, {
    key: 'handleChange',
    value: function handleChange(event) {
      this.setState(_defineProperty({}, event.target.name, event.target.value));
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        _lib.Grid,
        { className: '' + _CrimsonPrime2.default.loginContainer },
        _react2.default.createElement(
          _lib.Row,
          { className: 'center' },
          _react2.default.createElement(
            'h1',
            null,
            'Register'
          )
        ),
        _react2.default.createElement('input', {
          type: 'text',
          placeholder: 'User Name',
          name: 'username',
          value: this.state.username,
          onChange: this.handleChange
        }),
        _react2.default.createElement('input', {
          type: 'text',
          placeholder: 'Password',
          name: 'password',
          value: this.state.password,
          onChange: this.handleChange
        }),
        _react2.default.createElement(
          'button',
          { type: 'submit', onClick: this.submit },
          'Submit'
        )
      );
    }
  }]);

  return Register;
}(_react.Component);

exports.default = (0, _reactRedux.connect)(_Register.mapStateToProps, _Register.mapDispatchToProps)(Register);


Register.defaultProps = {};

Register.propTypes = {
  addUser: _react.PropTypes.func
};