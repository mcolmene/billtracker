'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _BillTracker = require('./components/BillTracker');

var _BillTracker2 = _interopRequireDefault(_BillTracker);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _BillTracker2.default;