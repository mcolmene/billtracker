'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.mapDispatchToProps = exports.mapStateToProps = undefined;

var _bills = require('../actions/bills');

var _addItem2 = require('../actions/addItem');

var mapStateToProps = exports.mapStateToProps = function mapStateToProps(state) {
  var _state$bills$toJS = state.bills.toJS(),
      bills = _state$bills$toJS.bills,
      hasErrored = _state$bills$toJS.hasErrored,
      isLoading = _state$bills$toJS.isLoading;

  return {
    bills: bills,
    hasErrored: hasErrored,
    isLoading: isLoading
  };
};

var mapDispatchToProps = exports.mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    getBills: function getBills(url) {
      return dispatch((0, _bills.getBills)(url));
    },
    addItem: function addItem(url, itemObject) {
      return dispatch((0, _addItem2.addItem)(url, itemObject));
    }
  };
};