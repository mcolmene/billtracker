'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.mapDispatchToProps = exports.mapStateToProps = undefined;

var _addUser2 = require('../actions/addUser');

var mapStateToProps = exports.mapStateToProps = function mapStateToProps(state) {
  var _state$addUser$toJS = state.addUser.toJS(),
      hasErrored = _state$addUser$toJS.hasErrored,
      isLoading = _state$addUser$toJS.isLoading;

  return {
    hasErrored: hasErrored,
    isLoading: isLoading
  };
};

var mapDispatchToProps = exports.mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    addUser: function addUser(url, username, password) {
      return dispatch((0, _addUser2.addUser)(url, username, password));
    }
  };
};