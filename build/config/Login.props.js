'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.mapDispatchToProps = exports.mapStateToProps = undefined;

var _login2 = require('../actions/login');

var _immutable = require('immutable');

var mapStateToProps = exports.mapStateToProps = function mapStateToProps(state) {
  var _state$login$toJS = state.login.toJS(),
      hasErrored = _state$login$toJS.hasErrored,
      isLoading = _state$login$toJS.isLoading,
      validLogin = _state$login$toJS.validLogin,
      loginAttempted = _state$login$toJS.loginAttempted;

  return {
    hasErrored: hasErrored,
    isLoading: isLoading,
    loginAttempted: loginAttempted,
    validLogin: validLogin
  };
};

var mapDispatchToProps = exports.mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    login: function login(url, username, password) {
      return dispatch((0, _login2.login)(url, username, password));
    }
  };
};