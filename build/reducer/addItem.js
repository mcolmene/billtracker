'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.addItem = addItem;

var _immutable = require('immutable');

var defaultState = (0, _immutable.fromJS)({
  hasErrored: false,
  isLoading: false
});

function addItem() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : defaultState;
  var action = arguments[1];

  switch (action.type) {
    case 'ADD_ITEM_FAILURE':
      {
        return state.set('hasErrored', action.hasErrored);
      }
    case 'ADD_ITEM_PENDING':
      {
        return state.set('isLoading', action.isLoading);
      }
    default:
      return state;
  }
}