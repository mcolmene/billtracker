'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.users = users;

var _immutable = require('immutable');

var defaultState = (0, _immutable.fromJS)({
  hasErrored: false,
  isLoading: false,
  users: []
});

function users() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : defaultState;
  var action = arguments[1];

  switch (action.type) {
    case 'FETCH_USERS_DATA_ERROR':
      {
        return state.set('hasErrored', action.hasErrored);
      }
    case 'USERS_ARE_LOADING':
      {
        return state.set('isLoading', action.isLoading);
      }
    case 'FETCH_USERS_DATA_SUCCESS':
      {
        return state.set('users', action.users);
      }
    default:
      return state;
  }
}