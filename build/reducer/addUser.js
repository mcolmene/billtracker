'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.addUser = addUser;

var _immutable = require('immutable');

var defaultState = (0, _immutable.fromJS)({
  hasErrored: false,
  isLoading: false
});

function addUser() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : defaultState;
  var action = arguments[1];

  switch (action.type) {
    case 'ADD_USER_FAILURE':
      {
        return state.set('hasErrored', action.hasErrored);
      }
    case 'ADD_USER_PENDING':
      {
        return state.set('isLoading', action.isLoading);
      }
    case 'ADD_USER_SUCCESS':
      {
        return state;
      }
    default:
      return state;
  }
}