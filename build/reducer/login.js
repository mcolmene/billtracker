'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.login = login;

var _immutable = require('immutable');

var defaultState = (0, _immutable.fromJS)({
  hasErrored: false,
  isLoading: false,
  loginAttempted: false,
  validLogin: false
});

function login() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : defaultState;
  var action = arguments[1];

  switch (action.type) {
    case 'LOGIN_FAILURE':
      {
        return state.set('hasErrored', action.hasErrored);
      }
    case 'LOGIN_PENDING':
      {
        return state.set('isLoading', action.isLoading);
      }
    case 'LOGIN_ATTEMPTED':
      {
        return state.set('loginAttempted', action.loginAttempted);
      }
    case 'LOGIN_SUCCESS':
      {
        console.log(state);
        return action.valid ? state.set('validLogin', action.valid) : state.set('validLogin', action.valid);
      }
    default:
      return state;
  }
}