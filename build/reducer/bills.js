'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.bills = bills;

var _immutable = require('immutable');

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var defaultState = (0, _immutable.Map)({
  hasErrored: false,
  isLoading: false,
  bills: []
});

function bills() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : defaultState;
  var action = arguments[1];

  switch (action.type) {
    case 'FETCH_BILLS_DATA_ERROR':
      {
        return state.set('hasErrored', action.hasErrored);
      }
    case 'BILLS_ARE_LOADING':
      {
        return state.set('isLoading', action.isLoading);
      }
    case 'FETCH_BILLS_DATA_SUCCESS':
      {
        return state.set('bills', action.bills);
      }
    case 'ADD_ITEM_SUCCESS':
      {
        var billList = state.get('bills');
        billList.push(action.item);
        return state.set('bills', [].concat(_toConsumableArray(billList)));
      }
    default:
      return state;
  }
}