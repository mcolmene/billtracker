'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _redux = require('redux');

var _reactRouterRedux = require('react-router-redux');

var _users = require('./users');

var _addUser = require('./addUser');

var _login = require('./login');

var _bills = require('./bills');

var _addItem = require('./addItem');

exports.default = (0, _redux.combineReducers)({
  users: _users.users,
  addUser: _addUser.addUser,
  login: _login.login,
  bills: _bills.bills,
  addItem: _addItem.addItem,
  routing: _reactRouterRedux.routerReducer
});