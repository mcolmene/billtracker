import React from 'react';
import { render } from 'react-dom';
import { Router, Route, IndexRoute, hashHistory} from 'react-router';

import Container from './src/components/Container'
import Login from './src/components/login/Login'
import Register from './src/components/register/Register'
import NotFound from './src/components/NotFound'

import BillTracker from './src';
import { Provider } from 'react-redux';
import store, { history } from './src/store';


render(
  <Provider store={store}>
    <Router history={hashHistory}>
      <Route path="/" component={Container} >
        <IndexRoute component={BillTracker}/>
        <Route path="/login" component={Login}/>
        <Route path="/register" component={Register}/>
      </Route>
      <Route path="*" component={NotFound} />
    </Router>
  </Provider>,
    document.getElementById('root')
);
