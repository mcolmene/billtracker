//server.js
'use strict'
//first we import our dependencies…
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
//and create our instances
const app = express();
const router = express.Router();
const mongo = require('mongodb').MongoClient;
const url = "mongodb://mcolmene:mc225967@ds157040.mlab.com:57040/bills";
//set our port to either a predetermined port number if you have set
//it up, or 3001
const port = process.env.API_PORT || 3000;

mongoose.connect(url, (err, db) => {
  if (err) return console.log(err);
  app.listen(port, function() {
    console.log("server running...")
  });
});
const userSchema = new mongoose.Schema({
  username: String,
  password: String
});
const billSchema = new mongoose.Schema({
  item: String,
  type: String,
  amount: String,
  description: String,
  date: String,
  user: String
});
const User = mongoose.model('User', userSchema);
const Bill = mongoose.model('Bill', billSchema);

//now we should configure the API to use bodyParser and look for
//JSON data in the request body
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
//To prevent errors from Cross Origin Resource Sharing, we will set
//our headers to allow CORS with middleware like so:
app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Credentials', 'true');
  res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
//and remove cacheing so we get the most recent comments
  res.setHeader('Cache-Control', 'no-cache');
  next();
});
//now we can set the route path & initialize the API
app.get('/', function(req, res) {
  const resultArray = [];
  mongo.connect(url, function(err, db) {
    if (err) return console.log(err);
    const cursor = db.collection('users').find();
    cursor.forEach(function(doc, err) {
      console.log('this is doc', doc);
      if (err) return console.log(err);
      resultArray.push(doc);
    }, function() {
      db.close();
    })
  });
  res.json({ message: resultArray});
});
app.post('/adduser', (req, res) => {
  mongo.connect(url, (err, db) => {
    if(err) return console.log(err);
    const user = new User({
      username: req.body.username,
      password: req.body.password
    })
    user.save( (err, result) => {
      if (err) return console.log(err);
      console.log('saved to database');
      db.close();
      res.redirect('/')
    })
  })
});
app.get('/users', (req, res) => {
  const resultArray = [];
  mongo.connect(url, function(err, db) {
    if (err) return console.log(err);
    const cursor = db.collection('users').find();
    cursor.forEach(function(doc, err) {
      if (err) return console.log(err);
      resultArray.push(doc);
    }, function() {
      db.close();
      res.json({ users: resultArray});
      console.log(resultArray);
    })
  })
});
app.post('/login', (req, res) => {
  const obj = {
    username: req.body.username,
    password: req.body.password
  };
  mongo.connect(url, function(err, db) {
    User.find(obj, (err, result) => {
      if (err) return console.log(err);
      console.log((result));
      console.log((result.length));
      (result.length === 0)
        ? res.json({credentials: 'invalid'})
        : res.json({credentials: 'valid'})
    }, () => {
      db.close();
    })
  })
});
app.get('/bills', (req, res) => {
  const resultArray = [];
  mongo.connect(url, function(err, db) {
    if (err) return console.log(err);
    const cursor = db.collection('bills').find();
    cursor.forEach(function(doc, err) {
      if (err) return console.log(err);
      resultArray.push(doc);
    }, function() {
      db.close();
      res.send(resultArray);
      console.log(resultArray);
    })
  })
});
app.post('/bills', (req, res) => {
  mongo.connect(url, (err, db) => {
    if(err) return console.log(err);
    const bill = new Bill({
      item: req.body.item,
      type: req.body.type,
      amount: req.body.amount,
      description: req.body.description,
      date: req.body.date,
      user: req.body.date
    });
    bill.save( (err, result) => {
      if (err) return console.log(err);
      console.log('saved to database');
      db.close();
      res.redirect('/')
    })
  })
});

